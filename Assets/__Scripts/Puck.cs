// #define SHOW_DEBUG_MESSAGES

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Puck : MonoBehaviour {
    public float initialVelocity = 10;
    public float yMin            = -10;

    private Rigidbody2D rigid;
    
    // Start is called before the first frame update
    void Start() {
        rigid = GetComponent<Rigidbody2D>();
        // Set initial velocity
        Vector2 vel = Vector2.down;
        vel.x = Random.Range( -1f, 1 );
        vel.Normalize();
        vel *= initialVelocity;
        rigid.velocity = vel;
    }

    public float speed {
        get { return rigid.velocity.magnitude; }
        set {
            Vector2 vel = rigid.velocity;
            vel = vel.normalized * value;
            rigid.velocity = vel;
#if SHOW_DEBUG_MESSAGES
            Debug.Log( $"New speed is: {value}" );
#endif
        }
    }

    public Vector2 velocity {
        get { return rigid.velocity; }
        set { rigid.velocity = value; }
    }

    private void FixedUpdate() {
        if ( transform.position.y < yMin ) {
            SceneManager.LoadScene( 0 );
        }
    }
}
