using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bricklayer : MonoBehaviour {
    public GameObject brickPrefab;
    [Range(1,11)]
    public int        width;
    [Range(1,10)]
    public int        height;
    public bool       stagger = false;

    private Transform trans;
    
    void Start() {
        trans = transform;
        Layout();
    }


    void Layout() {
        GameObject brickGO;
        int rowWidth = width;
        float minX = -(width - 1) / 2f;
        Vector3 pos;
        
        for (int h = 0; h < height; h++) {
            if ( stagger ) {
                rowWidth = (h % 2 == 0) ? width - 1 : width;
                minX = -(rowWidth - 1) / 2f;
            }
            for (int w = 0; w < rowWidth; w++) {
                // Instantiate a brick
                brickGO = Instantiate<GameObject>( brickPrefab, trans );
                pos = Vector3.zero;
                pos.x = minX + w;
                pos.y = h * 0.5f;
                brickGO.transform.localPosition = pos;
            }
        }
    }
    
    

    // Update is called once per frame
    void Update() { }
}
