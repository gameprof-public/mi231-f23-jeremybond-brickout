using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {
    public float xLimit               = 4.75f;
    public float puckVelIncreaseOnHit = 2f;

    void Start() {
        SetPaddlePosition();
    }

    void FixedUpdate() {
        SetPaddlePosition();
    }

    void SetPaddlePosition() {
        // Follow the Mouse
        Vector3 mousePosScreen = Input.mousePosition;
        mousePosScreen.z = -Camera.main.transform.position.z;
        Vector3 mousePos3D = Camera.main.ScreenToWorldPoint( mousePosScreen );

        // Limit X position
        mousePos3D.x = Mathf.Clamp( mousePos3D.x, -xLimit, xLimit );
        
        // Set the position of the paddle
        Vector3 pos = transform.position;
        pos.x = mousePos3D.x;
        transform.position = pos;
    }

    void OnCollisionEnter2D(Collision2D coll) {
        Puck puck = coll.collider.GetComponent<Puck>();
        if ( puck == null ) puck = coll.otherCollider.GetComponent<Puck>();
        if ( puck == null ) return;
        puck.speed += puckVelIncreaseOnHit;

        Vector2 deltaToPuck = puck.transform.position - transform.position;
        deltaToPuck.Normalize();
        deltaToPuck *= puck.speed;
        puck.velocity = deltaToPuck;
    }
}
