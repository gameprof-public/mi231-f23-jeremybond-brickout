using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Brick : MonoBehaviour {
    static public List<Brick> BRICK_LIST = new List<Brick>();

    void Start() {
        BRICK_LIST.Add( this );
    }

    private void OnCollisionEnter2D( Collision2D coll ) {
        if ( coll.gameObject.GetComponent<Puck>() == null ) return;
        Destroy( gameObject ); // Destroy this Brick
    }

    void OnDestroy() {
        BRICK_LIST.Remove( this );
        // Debug.Log( $"There are {BRICK_LIST.Count} bricks left!" );

        if ( BRICK_LIST.Count == 0 ) {
            // Reset the game!
            SceneManager.LoadScene( 0 );
        }
    }
}
